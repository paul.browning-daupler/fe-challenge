import os
import psycopg2
from flask import Flask, send_from_directory

print(os.environ.get("STATIC_PATH"))

app = Flask(
    __name__,
    static_url_path="",
    static_folder=os.environ.get("STATIC_PATH"),
    )

def get_db_connection():
    conn = psycopg2.connect(host="localhost",
                            port=os.environ["PGPORT"],
                            database=os.environ["PGDATABASE"],
                            user=os.environ["PGUSER"],
                            password=os.environ["PGPASSWORD"])
    return conn

@app.route("/send-request", methods=["POST"])
def send_request():
    return

@app.route("/")
def hello_world():
    return send_from_directory("../public", "index.html")

if __name__ == "__main__":
    app.run(debug=True)

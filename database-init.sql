CREATE TABLE contact (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  phone_number varchar(255) NOT NULL
);

CREATE TABLE request (
  id SERIAL PRIMARY KEY,
  contact_id SERIAL NOT NULL,
  description varchar(255) NOT NULL,
  CONSTRAINT fk_contact
    FOREIGN KEY(contact_id)
      REFERENCES contact(id)
);

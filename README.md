# Daupler Frontend Challenge

## Preqrequisites
- node@lts
- npm@8
- docker
- docker-compose

## Running the Project

1. Create a .env file with the following values populated:
    ```
    PGUSER=<username>
    PGPASSWORD=<password>
    PGDATABASE=daupler
    PGPORT=5432
    ```
    Note: If solving with Node, add `PORT=5000` to the `.env` file as well.
1. Install the server's dependencies:
    1. For node, run `npm ci` 
    1. For python:
        1. run `python3 -m venv venv`
        1. run `. venv/bin/activate`
        1. run `pip install --no-cache-dir --upgrade pip && pip install -r ./requirements.txt`
1. Stand up the database and Flask app with `docker-compose --env-file ./.env up --build --force-recreate --no-deps -d`
1. Start the application server:
    1. For node, run `npm start`
    1. For python, run `STATIC_PATH="$(pwd)/public" FLASK_APP=./src-python/app FLASK_ENV=development flask run`
1. The database is now available at localhost:5432 and the server is available at localhost:5000
1. When done, exit node & run `docker-compose stop`

## Database

The challenge contains a docker container with the following tables:
```
 -----------------------------
| Request                     |
 -----------------------------
| pk int          id          |
| fk int          contact_id  |
|    varchar(255) description |
 -----------------------------

 ------------------------------
| Contact                      |
 ------------------------------
| pk int          id           |
|    varchar(255) name         |
|    varchar(255) phone_number |
 ------------------------------
```

## Challenge

This app collects user contact information and a description of their issue. Complete the following tasks to make it function correctly:

- [ ] Send form data to the backend
- [ ] Receive and persist the data into the database
- [ ] Display a success message in the front end
- [ ] Style the page to match the mock-up ![](./mock-up.png)

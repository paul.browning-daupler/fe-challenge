const express = require('express');
const sendRequest = require('./send-request');

const app = express();
app.use(express.static('public'));

app.post('/send-request', sendRequest);

app.listen(process.env.PORT, () => {
  console.log(`Listening at http://localhost:${process.env.PORT}`);
});
